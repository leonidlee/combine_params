#!/env/python

from datetime import datetime


time1 = datetime.now()

params3 = {"p1": ["a", "b"], "p2": ["c", "d", "e"], "p3": ["f"]}

params = {
    'p4':['1', '2', '3', '4', '5', '6']
    ,'p5':['1', '2', '3', '4', '5', '6', '7']
    ,'p6':['1', '2', '3', '4', '5', '6', '7', '8']
    ,'p7':['1', '2', '3', '4', '5', '6', '7', '8', '9']
    ,'p8':['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
    ,'p9':['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']
}


out_list = [""]

for key,val in params.items():
  for v in val:
    z = (key+"="+v)
    out_list.append(z)
    for t in out_list:
      if (key not in t) and (t or (z not in out_list)):
        out_list.append(t+"&"+z)

print(datetime.now() -time1)
#print(out_list)

print(len(out_list))

